# Docker Registry

<div>
    <a href="https://www.docker.com/">
        <img src="https://img.shields.io/badge/docker-latest-blue">
    </a>
    <a href="https://docs.docker.com/compose/">
        <img src="https://img.shields.io/badge/docker%20compose-latest-blue">
    </a>
</div>

## 簡介

docker registry 是存放 docker image 地方，可以將打包之 docker image 存放至 docker registry，必要時候抓取 docker registry 倉庫映像檔，完成佈署任務

## 產生 ssl 公鑰與私鑰

```bash
openssl req -x509 -newkey rsa:2048 -keyout localhost_private_key.pem -out localhost_certificate.pem -nodes -days 365 -subj "/"
```

# 產生 docker registry 帳號密碼

```bash
htpasswd -Bbc .htpasswd [username] [password]
```

## browser 登入 docker registry 查看 _catalog 網址

```bash
https://x.x.x.x:5000/v2/_catalog
```

## browser 登入 docker registry 查看倉庫 images 清單網址

```bash
http://x.x.x.x:8080
```

## Docker 登入 docker registry

```bash
docker login x.x.x.x:5000
```

## Docker 上傳 images

```bash
docker push x.x.x.x:5000/alpine:latest
```
